package merlingwe.ouestlabonnebiere;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class BeerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent i = getIntent();
        String name = i.getStringExtra("beerName");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beer);

        TextView t = new TextView(this);
        t = (TextView)findViewById(R.id.txt_beerName);
        t.setText(name);
    }
}
