package merlingwe.ouestlabonnebiere.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import merlingwe.ouestlabonnebiere.OelbbApplication;
import merlingwe.ouestlabonnebiere.R;
import merlingwe.ouestlabonnebiere.objects.BeersList;
import merlingwe.ouestlabonnebiere.requests.BeersRequest;

public class RobospiceTest extends AppCompatActivity {
    protected SpiceManager spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);

    @Override
    protected void onStart() {
        super.onStart();
        spiceManager.start(this);
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_robospice_test);
    }

    public void download(View v) {
        OelbbApplication.getInstance().setCredentials(Base64.encodeToString("user:Password42".getBytes(), Base64.DEFAULT));
        BeersRequest request = new BeersRequest();
        String lastRequestCacheKey = request.createCacheKey();

        spiceManager.execute(request, lastRequestCacheKey, DurationInMillis.ONE_MINUTE, new BeersListRequestListener());



    }

    public class BeersListRequestListener implements RequestListener<BeersList> {

        public BeersListRequestListener() {
        }

        @Override
        public void onRequestFailure(SpiceException e) {
            TextView tv = (TextView) findViewById(R.id.textView);
            tv.setText("Request failed");
            Log.i("MDRR", "Failure");
        }

        @Override
        public void onRequestSuccess(BeersList listBeers) {
            //update your UI
            TextView tv = (TextView) findViewById(R.id.textView);
            tv.setText(listBeers.get(0).getName());
            Log.i("MDRR", "Success");
        }

    }
}
