package merlingwe.ouestlabonnebiere.objects;

import android.util.Log;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by Quentin on 18/05/2016.
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class Beer {

    private int id;
    private String name;
    private String description;
    private float alchohol;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getAlchohol() {
        return alchohol;
    }

    public void setAlchohol(float alchohol) {
        this.alchohol = alchohol;
    }
}