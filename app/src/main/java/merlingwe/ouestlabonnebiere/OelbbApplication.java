package merlingwe.ouestlabonnebiere;

import android.app.Application;

/**
 * Created by Quentin on 28/05/2016.
 */
public class OelbbApplication extends Application {

    private String credentials;
    private static OelbbApplication instance = null;
    public void onCreate() {
        super.onCreate();
        OelbbApplication.instance = this;
    }

    public static OelbbApplication getInstance() {
        return instance;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }
}
