package merlingwe.ouestlabonnebiere.requests;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

import org.springframework.http.HttpMethod;

import merlingwe.ouestlabonnebiere.objects.BeersList;

/**
 * Created by Quentin on 18/05/2016.
 */
public class BeersRequest extends BaseRequest<BeersList> {

    public BeersRequest() {

        super(BeersList.class, HttpMethod.GET, "beers");

    }

    /**
     * This method generates a unique cache key for this request. In this case
     * our cache key depends just on the keyword.
     * @return
     */
    public String createCacheKey() {
        return "beers";
    }
}
