package merlingwe.ouestlabonnebiere.requests;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;

import merlingwe.ouestlabonnebiere.OelbbApplication;

/**
 * Created by Quentin on 28/05/2016.
 */
public class BaseRequest<T> extends SpringAndroidSpiceRequest<T> {
    protected String url;
    protected HttpMethod method;
    protected HttpEntity<?> requestEntity;
    protected Class<T> clazz;

    public BaseRequest(Class<T> clazz, HttpMethod method, String url) {
        super(clazz);
        this.method = method;
        this.clazz = clazz;
        this.url = "http://oelbb.colocation.tk/"+url;
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Basic "+ OelbbApplication.getInstance().getCredentials());
        this.requestEntity = new HttpEntity<Object>(headers);

    }

    @Override
    public T loadDataFromNetwork() throws Exception {
        return getRestTemplate().exchange(url, method, requestEntity, clazz).getBody();
    }
}
